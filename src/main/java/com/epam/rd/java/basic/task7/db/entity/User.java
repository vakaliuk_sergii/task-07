package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public User() {}

	public User(int id, String login) {
		this.id = id;
		this.login = login; }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		User user = new User();
		user.login = login;
		return user;
	}

	@Override
	public String toString() { return login; }

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		User user = (User) obj;
		return login.equals(user.login);
	}

}
