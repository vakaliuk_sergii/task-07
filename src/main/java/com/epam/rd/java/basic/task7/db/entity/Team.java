package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public Team() {}

	public Team(int id, String name) {
		this.id = id;
		this.name = name;}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.name = name;
		return team;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Team team = (Team) obj;
		return name.equals(team.name);
	}

}
