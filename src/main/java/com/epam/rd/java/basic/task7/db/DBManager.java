package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final DBManager instance = new DBManager();
	private static final Properties properties = new Properties();

	static {
		try {
			properties.load(new FileReader("app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users");
			ResultSet resultSet = preparedStatement.executeQuery()) {
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("Exception while finding all users", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try(Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users (login) VALUES (?)", Statement.RETURN_GENERATED_KEYS)) {
		preparedStatement.setString(1, user.getLogin());
		if (preparedStatement.executeUpdate() > 0) {
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				user.setId(resultSet.getInt(1));
				return true;
			}
		}
		return false;
		} catch (SQLException e) {
			throw new DBException("Exception while inserting user", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM users WHERE id = ? AND login = ?")){
			for (User user : users) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setString(2, user.getLogin());
				preparedStatement.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			throw new DBException("Exception while deleting users", e);
		}
	}

	public User getUser(String login) throws DBException {
		User user = null;
		try (Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE login = ?")){
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
			}
		} catch (SQLException e) {
			throw new DBException("Exception while getting user", e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try (Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			 PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams WHERE name = ?")){
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
			}
		} catch (SQLException e) {
			throw new DBException("Exception while getting team", e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM teams");
			ResultSet resultSet = preparedStatement.executeQuery()) {
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Exception while finding all teams", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			preparedStatement = connection.prepareStatement("INSERT INTO teams (id, name) VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());
			if (preparedStatement.executeUpdate() > 0) {
				resultSet = preparedStatement.getGeneratedKeys();
				if (resultSet.next()) {
					team.setId(resultSet.getInt(1));
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			throw new DBException("Exception while inserting team", e);
		}
		finally {
			close(resultSet);
			close(preparedStatement);
			close(connection);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)");
			for (Team team : teams) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.executeUpdate();
			}
			connection.commit();
			return true;
		} catch (SQLException e) {
			if (connection != null) {
				try {
					connection.rollback();
					throw new DBException("Exception while setting teams for user", e);
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return false;
	}

	private void close(AutoCloseable preparedStatement) {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			preparedStatement = connection.prepareStatement("SELECT teams.id, teams.name FROM teams JOIN users_teams ON teams.id = users_teams.team_id WHERE users_teams.user_id = ?");
			preparedStatement.setInt(1, user.getId());
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("Exception while finding all teams", e);
		} finally {
			close(resultSet);
			close(preparedStatement);
			close(connection);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			preparedStatement = connection.prepareStatement("DELETE FROM teams WHERE id = ? AND name = ?");
			preparedStatement.setInt(1, team.getId());
			preparedStatement.setString(2, team.getName());
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException e) {
			throw new DBException("Exception while deleting team", e);
		} finally {
			close(preparedStatement);
			close(connection);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
			preparedStatement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id = ?");
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Exception while updating team", e);
		} finally {
			close(preparedStatement);
			close(connection);
		}
		return true;
	}

}
